(function() {
    'use strict';

  angular
      .module('app')
      .controller('CalculatorController', CalculatorController);

      CalculatorController.$inject = ['$scope', 'InitialFactory'];

  function CalculatorController($scope, InitialFactory) {

    activate();
    function activate() {
      $scope.actions = '';
      $scope.values = [];
      $scope.result = 0;
      $scope.output = 0; // var to display current action on view

      // method to track current action in $scope.actions
      $scope.initialValue = function(clickedValue){
        $scope.actions += clickedValue;
        $scope.output = $scope.actions;
      };

      // method to remove unneeded data from array
      // haven't found how to replace this method with something else correctly
      // this one is very suit
      Array.prototype.remove = function() {
        var what, a = arguments, L = a.length, ax;
        while (L && this.length) {
          what = a[--L];
          while ((ax = this.indexOf(what)) !== -1) {
            this.splice(ax, 1);
          }
        }
        return this;
      };

      // method to convert dots and elements from general array to numbers with dots
      // as an example: ['5', '.', '3'] becomes ['5.3']
      $scope.checkDots = function(val){
        if(val){
          for(var i = 0, len = val.length; i < len; i++){
            if(val[i].indexOf('.') != -1){
              val[i] = val[i-1] + val[i] + val[i+1];
              val.splice(i-1, 1, null);
              val.splice(i+1, 1, null);
            }
          }
          val.remove(null);
        }
        return val;
      };

      // method parse every element from array with data and converts it to Number
      $scope.convertToInteger = function(val){
        for(var i = 0; i < val.length; i++) {
          if(parseFloat(val[i])){
            val[i] = parseFloat(val[i]);
          }
        }
        return val;
      };

      // this method will be called after every event, it is used to store all filled
      // data in array, refresh temporary variable and prepare the array for any action
      $scope.refreshData = function(){
        $scope.values.push($scope.actions);
        $scope.actions = '';
      };

      // this method will remove 'left' and 'right' values from array after performing any action
      // only 'center' value will be left, other will be unneeded after the action
      // example: ['5','+','3'] will become ['5','8','3'] after the 'plus' action and
      // left and right values will be incorrect after this case
      $scope.spliceAction = function(i){
        $scope.convertedData.splice(i-1, 1, null);
        $scope.convertedData.splice(i+1, 1, null);
        $scope.convertedData.remove(null);
      };

      // all events
      $scope.events = {
        plus: function(){
          $scope.refreshData();
          $scope.values.push('+');
        },
        minus: function(){
          $scope.refreshData();
          $scope.values.push('-');
        },
        multiply: function(){
          $scope.refreshData();
          $scope.values.push('*');
        },
        divide: function(){
          $scope.refreshData();
          $scope.values.push('/');
        },
        addDot: function(){
          $scope.refreshData();
          $scope.values.push('.');
        },
        equals: function(){
          if($scope.actions){
            $scope.values.push($scope.actions);
          }
          $scope.preparedData = $scope.checkDots($scope.values);
          $scope.convertedData = $scope.convertToInteger($scope.preparedData);
          // this will work until we have only one element in array - result
          while ($scope.convertedData.length !== 1){
            for(var i = 0, temp = $scope.convertedData.length; i < temp; i++){
              // here I'm looking for an 'action' item and performing logical action with it
              // after that all unneeded data is removed
                switch($scope.convertedData[i]) {
                  case "+":
        		    $scope.convertedData[i] = $scope.convertedData[i-1] + $scope.convertedData[i+1];
                    $scope.spliceAction(i);
                    break;
                  case "-":
                    $scope.convertedData[i] = $scope.convertedData[i-1] - $scope.convertedData[i+1];
                    $scope.spliceAction(i);
                    break;
                  case "*":
                    $scope.convertedData[i] = $scope.convertedData[i-1] * $scope.convertedData[i+1];
                    $scope.spliceAction(i);
                    break;
                  case "/":
                    $scope.convertedData[i] = $scope.convertedData[i-1] / $scope.convertedData[i+1];
                    $scope.spliceAction(i);
                    break;
                }
              }
              }
              // saving result
              $scope.result = $scope.convertedData[0];
              // and showing it on the view
              $scope.output = $scope.result;
              InitialFactory.saveResult($scope.result); // just sample of using factories
              this.refresh();
        },
        refresh: function(){
          $scope.values = [];
          $scope.actions = '';
        }
      };
    }
  }
})();
