(function() {
    'use strict';

  angular
      .module('app')
      .controller('InitialCtrl', InitialCtrl);

      InitialCtrl.$inject = ['InitialFactory', '$scope'];

  function InitialCtrl(InitialFactory, $scope) {

    activate();
    function activate() {
      $scope.engineers = InitialFactory.getNames();
    }
  }

})();
