angular
    .module('app')
    .directive('calculator', calculator);

function calculator() {
  var directive = {
      restrict: 'E',
      // used 'template' instead of 'templateUrl' in this case because I've had an error in
      // browser when I tried to load HTML file locally
      // Error: Failed to execute 'send' on 'XMLHttpRequest': Failed to load 'file'
      template: '<div class="container">\
          <form class="form-inline">\
            <div class="row">\
              <div>\
                <input type="text" class="form-control" value="{{output}}"></input>\
              </div>\
            </div>\
            <div class="row">\
              <a class="btn btn-lg btn-default" ng-click="initialValue(1)">1</a>\
              <a class="btn btn-lg btn-default" ng-click="initialValue(2)">2</a>\
              <a class="btn btn-lg btn-default" ng-click="initialValue(3)">3</a>\
              <a class="btn btn-lg btn-default" ng-click="events.plus()">+</a>\
            </div>\
            <div class="row">\
              <a class="btn btn-lg btn-default" ng-click="initialValue(4)">4</a>\
              <a class="btn btn-lg btn-default" ng-click="initialValue(5)">5</a>\
              <a class="btn btn-lg btn-default" ng-click="initialValue(6)">6</a>\
              <a class="btn btn-lg btn-default" ng-click="events.minus()">-</a>\
            </div>\
            <div class="row">\
              <a class="btn btn-lg btn-default" ng-click="initialValue(7)">7</a>\
              <a class="btn btn-lg btn-default" ng-click="initialValue(8)">8</a>\
              <a class="btn btn-lg btn-default" ng-click="initialValue(9)">9</a>\
              <a class="btn btn-lg btn-default" ng-click="events.multiply()">*</a>\
            </div>\
            <div class="row">\
              <a class="btn btn-lg btn-default" ng-click="events.refresh()">C</a>\
              <a class="btn btn-lg btn-default" ng-click="initialValue(0)">0</a>\
              <a class="btn btn-lg btn-default" ng-click="events.equals()">=</a>\
              <a class="btn btn-lg btn-default" ng-click="events.divide()">/</a>\
              <a class="btn btn-lg btn-default" ng-click="events.addDot()">.</a>\
            </div>\
          </form>\
        </div>\
',
      scope: false,
      controller: 'CalculatorController'
  };

  return directive;

}
