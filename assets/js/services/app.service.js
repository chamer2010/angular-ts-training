(function() {
    'use strict';

    angular
        .module('app')
        .factory('InitialFactory', InitialFactory);

    function InitialFactory() {
      // just saving latest value from calculator directive to show that factory is
      // implemented correctly
      return {
        saveResult: function(res){
          var result = [];
          result.push(res);
          return result;
        }
      }
    }
})();
